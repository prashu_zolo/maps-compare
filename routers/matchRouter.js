'use strict';
const matchRouter = require('express').Router();
const { MatchController } = require('../controllers');

matchRouter.post('/match', MatchController.post);

module.exports = matchRouter;
