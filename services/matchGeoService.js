const FuzzySet = require('fuzzyset');
const MapboxService = require('../services/mapboxService');
const GoogleMapsService = require('../services/googleMapsService');
const ElasticSearchService = require('../services/elasticSearchService');
const logger = require('../configs/loggerConfig');
const getDistanceFromLatLonInMeter = require('../utils/distanceUtil');

class MatchGeoService {
  static async match(keyword, placeIds) {
    const mapBoxResponse = await MapboxService.forward(keyword);
    const { rawBody: { features } } = mapBoxResponse;

    const googleMapsResults = await Promise.all(placeIds.map(async placeId => {
      const response = await GoogleMapsService.forward(placeId);
      return response.data.results[0];
    }));

    // const locationIqResults = await LocationIqService.forward(keyword);

    const matchDocument = {
      mapBox: features,
      // locationIq: locationIqResults,
      googleMaps: googleMapsResults,
    };

    const esResponse = await ElasticSearchService.indexDocument('match_results_geocode', matchDocument);
    logger.info(esResponse);

    this.fuzzyMatch(googleMapsResults, googleMapsResults);
  }

  static fuzzyMatch(googleMapsResults, mapBoxResults) {
    const fSet = FuzzySet();
    const matchResults = Array();
    googleMapsResults.forEach(googleMapsResult => fSet.add(googleMapsResult.formatted_address));

    mapBoxResults.forEach(mapBoxResult => {
      const fuzzyMatches = fSet.get(mapBoxResult.place_name);

      fuzzyMatches.forEach(match => {
        logger.info('**** Matching Lat Long ****');
        logger.info(match);
        const [score, matchedValue] = match;
        if ((score * 100) > 90) {
          const googleMapsResult = googleMapsResults.find(res => res.formatted_address === matchedValue);

          const [gLat, gLong] = googleMapsResult.location;
          const [mLat, mLong] = mapBoxResult.coordinates;
          const distance = getDistanceFromLatLonInMeter(gLat, gLong, mLat, mLong);
          logger.info(distance);
          matchResults.push({
            mapbox: mapBoxResult,
            match: (distance < 100),
            googleMap: googleMapsResult,
          });
        }
      });
    });
  }
}

module.exports = MatchGeoService;
