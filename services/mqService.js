const amqp = require('amqplib');
const MatchGeoService = require('./matchGeoService');
const configs = require('../configs');

amqp.connect(configs.ampq.host, (err, conn) => {
  conn.createChannel((err, ch) => {
    ch.assertQueue(configs.ampq.host, { durable: true });

    ch.consume(
      configs.ampq.queue,
      msg => processMessage(msg),
      { noAck: true },
    );

    const processMessage = async function (message) {
      const { keyword, placeIds } = message;
      await MatchGeoService.match(keyword, placeIds);
    };
  });
});
