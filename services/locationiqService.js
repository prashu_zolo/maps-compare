'use strict';
const configs = require('../configs');
const { LocationIq } = require('../locationiq');

const client = new LocationIq(configs.locationIq);

class LocationIqService {
  static async forward(keyword, countryCodes = 'in') {
    return await client.search({
      // eslint-disable-next-line id-length
      q: keyword,
      countrycodes: countryCodes,
    });
  }

  static async reverse(latitude, longitude) {
    return await client.reverse({
      lat: latitude,
      lon: longitude,
    });
  }
}

module.exports = LocationIqService;
