'use strict';
const configs = require('../configs');
const { Client } = require('@elastic/elasticsearch');

const client = new Client(configs.elasticsearch);

// eslint-disable-next-line no-unused-vars
client.cluster.health({}, (err, resp, status) => {
  console.log('-- Client Health --', resp);
});

class ElasticSearchService {
  static async indexDocument(indexName, data) {
    if (!client.indices.exists({ index: indexName })) {
      await this.createIndex(indexName);
    }

    await client.index({
      index: indexName,
      body: data,
    });
  }

  static async createIndex(indexName) {
    await client.indices.create({
      index: indexName,
    });
  }
}

module.exports = ElasticSearchService;
