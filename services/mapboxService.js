'use strict';
const configs = require('../configs');

const Mapbox = require('@mapbox/mapbox-sdk');
const MapBoxGeoCoding = require('@mapbox/mapbox-sdk/services/geocoding');

const MapBoxClient = new Mapbox(configs.mapbox);
const MapBoxGeoCodingClient = MapBoxGeoCoding(MapBoxClient);

class MapboxService {
  static async forward(keyword, countries = ['in']) {
    return await MapBoxGeoCodingClient.forwardGeocode({ query: keyword, countries }).send();
  }

  static async reverse(latitude, longitude) {
    return MapBoxGeoCodingClient.reverseGeocode({
      query: [latitude, longitude],
    });
  }
}

module.exports = MapboxService;
