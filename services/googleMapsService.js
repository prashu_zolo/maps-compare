'use strict';
const configs = require('../configs');
const { Client } = require('@googlemaps/google-maps-services-js');

const client = new Client({});

class GoogleMapsService {
  static async forward(placeId) {
    return await client.geocode({
      params: {
        // eslint-disable-next-line camelcase
        place_id: placeId,
        key: configs.googleMaps.key,
      },
    });
  }

  static async reverse(latitude, longitude) {
    return await client.reverseGeocode({
      params: {
        key: configs.googleMaps.key,
        latlng: [latitude, longitude],
      },
    });
  }
}

module.exports = GoogleMapsService;
