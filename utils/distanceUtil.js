
const deg2Rad = deg => deg * (Math.PI / 180);

const getDistanceFromLatLonInMeter = (lat1, lon1, lat2, lon2) => {
  const radius = 6371;
  const dLat = deg2Rad(lat2 - lat1);
  const dLon = deg2Rad(lon2 - lon1);
  // eslint-disable-next-line max-len
  const cosine = (Math.sin(dLat / 2) * Math.sin(dLat / 2)) + (Math.cos(deg2Rad(lat1)) * Math.cos(deg2Rad(lat2)) *  Math.sin(dLon / 2) * Math.sin(dLon / 2));
  const tangent = 2 * Math.atan2(Math.sqrt(cosine), Math.sqrt(1 - cosine));
  return radius * tangent * 1000;
};

module.exports = getDistanceFromLatLonInMeter;
