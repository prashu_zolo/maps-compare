'use strict';
const rootPath = process.cwd();
require('dotenv-flow').config({ path: `${rootPath}/envs` });

const port = process.env.PORT;

const ampq = {
  host: `${process.env.AMPQ_HOST}`,
  queue: `${process.env.MAP_QUEUE}`,
};

const mapbox = {
  accessToken: `${process.env.MAPBOX_TOKEN}`,
};

const locationIq = {
  key: `${process.env.LOCATION_IQ_TOKEN}`,
  format: 'json',
};

const googleMaps = {
  key: `${process.env.GOOGLE_MAPS_API_KEY}`,
};

const elasticsearch = {
  node: `${process.env.ES_HOST}`,
};

module.exports = {
  ampq,
  port,
  mapbox,
  googleMaps,
  locationIq,
  elasticsearch,
};
