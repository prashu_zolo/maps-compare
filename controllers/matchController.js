'use strict';
const MatchGeoService = require('../services/matchGeoService');

class MatchController {
  static async post(req, res) {
    const { body: { keyword, placeIds } } = req;
    await MatchGeoService.match(keyword, placeIds);
    return res.ok({});
  }
}

module.exports = {
  MatchController,
};
