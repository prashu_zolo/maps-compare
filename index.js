'use strict';
const logger = require('log4js').getLogger('ENTRY.index');
const express = require('express');
const configs = require('./configs');
const app = express();
const server = require('http').createServer(app);
const { port } = configs;

app.use('/', require('./routers'));

server.listen(port, () => logger.info(`app listen ${port} port`));
