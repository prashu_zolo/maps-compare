'use strict';
const general = require('./general');
const errorMessages = require('./errorMessages');

module.exports = {
  general,
  errorMessages,
};
